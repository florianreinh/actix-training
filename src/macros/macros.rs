#[macro_export]
macro_rules! response_ok {
    ($($x:expr)?) => {
        $(
            return HttpResponse::Ok().body($x)
        )?
    };
}

#[macro_export]
macro_rules! response_bad_request {
    ($($x:expr)?) => {
        $(
            return HttpResponse::BadRequest().body($x)
        )?
    }
}
