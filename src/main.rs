mod macros;
mod maths;
mod websocket;

use crate::maths::functions::fib;
use crate::maths::functions2::fac;
use crate::websocket::wsimpl::go_ws;

use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

#[get("/fibreq/{n}")]
async fn fibreq(path: web::Path<(u32,)>) -> impl Responder {
    HttpResponse::Ok().body(fib(path.into_inner().0 as u128).to_string())
}

#[get("/facreq/{n}")]
async fn facreq(path: web::Path<(u8,)>) -> impl Responder {
    let facres = fac(path.into_inner().0 as u128);

    match facres {
        Ok(r) => response_ok!(r.to_string()),
        Err(err) => response_bad_request!(err),
    }
}

#[post("/echo")]
async fn echo(req_body: String) -> impl Responder {
    HttpResponse::Ok().body(req_body)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(hello)
            .service(echo)
            .service(fibreq)
            .service(facreq)
            .service(web::resource("/ws/").route(web::get().to(go_ws)))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
