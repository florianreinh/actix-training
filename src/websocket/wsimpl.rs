use crate::maths::functions::fib;
use actix::{Actor, StreamHandler};
use actix_web::{web, Error, HttpRequest, HttpResponse};
use actix_web_actors::ws;

struct MathWs;

impl Actor for MathWs {
    type Context = ws::WebsocketContext<Self>;
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for MathWs {
    fn handle(
        &mut self,
        msg: Result<ws::Message, ws::ProtocolError>,
        ctx: &mut <Self as actix::Actor>::Context,
    ) {
        match msg {
            Ok(ws::Message::Text(text)) => ctx.text(fib(text.parse::<u128>().unwrap()).to_string()),
            Ok(ws::Message::Ping(msg)) => ctx.pong(&msg),
            Ok(ws::Message::Binary(bin)) => ctx.binary(bin),
            _ => (),
        }
    }
}

pub async fn go_ws(req: HttpRequest, stream: web::Payload) -> Result<HttpResponse, Error> {
    let res = ws::start(MathWs {}, &req, stream);
    println!("{:?}", res);
    res
}
