pub fn fac(n: u128) -> Result<u128, &'static str> {
    match n {
        n if n > 34 => Err("Ohoh! n is too darn high!"),
        0 => Ok(1),
        _ => Ok(n * fac(n - 1).unwrap()),
    }
}
